package wakeOnLAN

import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
import java.net.NetworkInterface

fun main() {
    val macAddress = "A8:5E:45:59:98:9D" // target PC MAC address
    val port = 7 // Wake-on-LAN port
    val localBroadcastAddress = getBroadcastAddress() ?: InetAddress.getByName("192.168.0.255")
    sendWakeOnLanPacket(macAddress, localBroadcastAddress, port)
}

fun sendWakeOnLanPacket(targetMacAddress: String, localBroadcastAddress : InetAddress, port : Int) =
    try {
        val bytes = createMagicPacket(targetMacAddress)

        val magicPacket = DatagramPacket(bytes, bytes.size, localBroadcastAddress, port)

        DatagramSocket().run {
            send(magicPacket)
            close()
        }
    } catch (e : NumberFormatException){
        println(e.message)
    } catch (e : InvalidAddressException){
        println(e.message)
    } catch (e: Exception){
        println("Unexpected exception: ${e.message}")
    }

fun createMagicPacket(macAddress: String): ByteArray {
    val numberBaseFormat = 16
    val macBytes = macAddress.split(":").map { it.toInt(numberBaseFormat).toByte() }.toByteArray()
    if (macBytes.size != 6) throw InvalidAddressException("Mac packets must be 6 bytes")

    val numberOfFFs = 6
    val bytes = ByteArray(numberOfFFs + 16 * macBytes.size)

    // 6x 0xFF
    for (i in 0 until numberOfFFs) {
        bytes[i] = 0xFF.toByte()
    }

    // 16x target PC MAC address
    for (i in numberOfFFs until bytes.size) {
        bytes[i] = macBytes[(i - numberOfFFs) % macBytes.size]
    }

    return bytes
}

fun getBroadcastAddress(): InetAddress? {
    val networkInterfaces = NetworkInterface.getNetworkInterfaces()
    while (networkInterfaces.hasMoreElements()) {
        val networkInterface = networkInterfaces.nextElement()
        if (!networkInterface.isLoopback && networkInterface.isUp) {
            val interfaceAddresses = networkInterface.interfaceAddresses
            for (interfaceAddress in interfaceAddresses) {
                val broadcast = interfaceAddress.broadcast
                if (broadcast != null) {
                    return broadcast
                }
            }
        }
    }
    return null
}

class InvalidAddressException(message: String) : Exception(message)
